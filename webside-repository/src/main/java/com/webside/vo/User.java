package com.webside.vo;

import lombok.Data;

/**
 * @author wjggwm
 * @version 1.0.0
 * @date 2019/2/27  22:44
 */
@Data
public class User {

	private String name;

	private String sex;
}
