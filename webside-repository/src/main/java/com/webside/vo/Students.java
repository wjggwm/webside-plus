package com.webside.vo;

import com.webside.po.Student;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author wjggwm
 * @version 1.0.0
 * @date 2019/2/25  21:37
 */
@Data
@XmlRootElement(name = "Students")
public class Students {

	private List<Student> students;
}
