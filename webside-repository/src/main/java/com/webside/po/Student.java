package com.webside.po;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author wjggwm
 * @version 1.0.0
 * @date 2019/2/25  21:33
 */
@Data
@XmlRootElement(name = "Student")
@XmlAccessorType(XmlAccessType.FIELD)
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;
	@XmlElement
	private Integer id;
	@XmlElement
	private String name;
	@XmlElement
	private char sex;
	@XmlElement
	private String address;
	@XmlElement
	private Integer age;

}
