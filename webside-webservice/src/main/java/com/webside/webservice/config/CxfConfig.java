package com.webside.webservice.config;

import com.webside.webservice.server.DemoService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 *
 * @author wjggwm
 * @date 2019-02-19 21:23:58
 */
@Configuration
public class CxfConfig {

	@Autowired
	private Bus bus;

	@Bean(name = Bus.DEFAULT_BUS_ID)
	SpringBus springBus() {
		return new SpringBus();
	}

	@Autowired
	DemoService demoService;

	/**
	 * 此方法作用是改变项目中服务名的前缀名，此处127.0.0.1或者localhost不能访问时，请使用ipconfig查看本机ip来访问
	 * 此方法被注释后:wsdl访问地址为http://127.0.0.1:8080/services/demo?wsdl
	 * 去掉注释后：wsdl访问地址为：http://127.0.0.1:8080/soap/demo?wsdl
	 * 此处需要注意：dispatcherServlet 无法启动或会导致其他url无法访问，所以改为webServiceServlet
	 */
	@Bean
	public ServletRegistrationBean webServiceServlet() {
		return new ServletRegistrationBean(new CXFServlet(), "/soap/*");
	}

	/** JAX-WS
	 * 站点服务
	 * **/
	@Bean
	public Endpoint endpoint() {
		Endpoint endpoint = new EndpointImpl(springBus(), demoService);
		endpoint.publish("/demo");
		return endpoint;
	}

}
