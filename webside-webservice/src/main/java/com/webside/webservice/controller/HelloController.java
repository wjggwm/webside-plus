package com.webside.webservice.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author wjggwm
 * @version 1.0.0
 * @date 2019/2/19  22:58
 */
@RestController
@RequestMapping("/demo")
public class HelloController {

	@RequestMapping("/sayHello")
	public String sayHello(String name) {
		return name+":hello"+"("+new Date()+")";
	}

	@RequestMapping("/json")
	public String test(String json) {
		return json;
	}

}
