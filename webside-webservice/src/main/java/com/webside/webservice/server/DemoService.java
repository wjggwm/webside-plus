package com.webside.webservice.server;

import com.webside.po.Student;
import com.webside.vo.Students;
import com.webside.vo.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * @author wjggwm
 * @version 1.0.0
 * @date 2019/2/19  19:26
 */
@WebService
public interface DemoService {

	@WebMethod
	String sayHello(String user);

	@WebMethod
	Student getStudent(Integer id);

	@WebMethod
	Students getStudents();

	@WebMethod
	Student getStudentObject(String xml);

	@WebMethod
	Student getStudentObject1(Student student);

	@WebMethod
	User getUser(User user);

}
