package com.webside.webservice.server.impl;

import com.webside.po.Student;
import com.webside.vo.Students;
import com.webside.vo.User;
import com.webside.webservice.server.DemoService;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author wjggwm
 * @version 1.0.0
 * @date 2019/2/19  19:27
 */
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT,use=SOAPBinding.Use.LITERAL,parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
@WebService(name = "demoService",
		targetNamespace = "http://server.webservice.webside.com/",
		endpointInterface="com.webside.webservice.server.DemoService")
@Component
public class DemoServiceImpl implements DemoService {

	@Override
	public String sayHello(String user) {
		return user+":hello"+"("+new Date()+")";
	}

	@Override
	public Student getStudent(Integer id) {
		Student student = new Student();
		student.setId(id);
		student.setName("wjggwm");
		student.setAge(20);
		student.setSex('F');
		student.setAddress("阿卡迪亚");
		return student;
	}

	@Override
	public Students getStudents() {
		Students students = new Students();

		Student student = new Student();
		student.setId(1);
		student.setName("wjggwm");
		student.setAge(20);
		student.setSex('F');
		student.setAddress("阿卡迪亚");

		Student student1 = new Student();
		student1.setId(1);
		student1.setName("wjggwm");
		student1.setAge(20);
		student1.setSex('F');
		student1.setAddress("阿卡迪亚");

		List<Student> list = new ArrayList<>();
		list.add(student);
		list.add(student1);
		students.setStudents(list);
		return students;
	}

	@Override
	public Student getStudentObject(String xml) {
		return new Student();
	}

	@Override
	public Student getStudentObject1(Student student) {
		return student;
	}

	@Override
	public User getUser(User user) {
		return user;
	}
}
