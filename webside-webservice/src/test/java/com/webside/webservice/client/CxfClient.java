package com.webside.webservice.client;

import com.webside.po.Student;
import com.webside.util.HttpUtil;
import com.webside.webservice.server.DemoService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.jupiter.api.Test;

/**
 * webservice客户端：
 * 			       该类提供两种不同的方式来调用webservice服务
 * 				1：代理工厂方式
 * 				2：动态调用webservice
 *
 */
public class CxfClient {
	
	
	public static void main(String[] args) {
		CxfClient.main1();
		CxfClient.main2();
//		CxfClient.main3();
//		CxfClient.main4();
//		CxfClient.main5();
	}

	/**
	 * 1.代理类工厂的方式,需要拿到对方的接口地址
	 */
	public static void main1() {
		try {
			// 接口地址
//			String address = "http://127.0.0.1:8082/soap/demo?wsdl";
			String address = "http://127.0.0.1:8080/webservice/soap/demo?wsdl";
			// 代理工厂
			JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
			// 设置代理地址
			jaxWsProxyFactoryBean.setAddress(address);
			// 设置接口类型
			jaxWsProxyFactoryBean.setServiceClass(DemoService.class);
			// 创建一个代理接口实现
			DemoService us = (DemoService) jaxWsProxyFactoryBean.create();
			// 调用代理接口的方法调用并返回结果
			String result = us.sayHello("wjggwm");
			System.out.println("返回结果:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 2：动态调用
	 */
	public static void main2() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//		Client client = dcf.createClient("http://127.0.0.1:8082/soap/demo?wsdl");
		Client client = dcf.createClient("http://127.0.0.1:8080/webservice/soap/demo?wsdl");
		// 需要密码的情况需要加上用户名和密码
		// client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
		Object[] objects = new Object[0];
		try {
			// invoke("方法名",参数1,参数2,参数3....);
			objects = client.invoke("sayHello", "wjggwm");
			System.out.println("返回数据:" + objects[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 1.代理类工厂的方式,需要拿到对方的接口地址
	 */
	public static void main3() {
		try {
			// 接口地址
//			String address = "http://127.0.0.1:8082/soap/demo?wsdl";
			String address = "http://127.0.0.1:8080/webservice/soap/demo?wsdl";
			// 代理工厂
			JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
			// 设置代理地址
			jaxWsProxyFactoryBean.setAddress(address);
			// 设置接口类型
			jaxWsProxyFactoryBean.setServiceClass(DemoService.class);
			// 创建一个代理接口实现
			DemoService us = (DemoService) jaxWsProxyFactoryBean.create();
			// 调用代理接口的方法调用并返回结果
			Student student = us.getStudent(10001);
			System.out.println("返回结果:" + student.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 2：动态调用
	 */
	public static void main4() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//		Client client = dcf.createClient("http://127.0.0.1:8082/soap/demo?wsdl");
		Client client = dcf.createClient("http://127.0.0.1:8080/webservice/soap/demo?wsdl");
		// 需要密码的情况需要加上用户名和密码
		// client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
		Object[] object = new Object[0];
		try {
			// invoke("方法名",参数1,参数2,参数3....);
			object = client.invoke("getStudents");
			System.out.println("返回数据:" + object[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 2：动态调用
	 * https://www.cnblogs.com/lichmama/p/8728262.html
	 */
	public static void main5() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//		Client client = dcf.createClient("http://127.0.0.1:8082/soap/demo?wsdl");
		Client client = dcf.createClient("http://127.0.0.1:8080/webservice/soap/demo?wsdl");
		// 需要密码的情况需要加上用户名和密码
		// client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
		Object[] object = new Object[0];
		try {
			String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><Student><id>05</id><name>20160630083759</name><sex>F</sex><age>81</age><address>JT410100201606306418 </address></Student>";
			// invoke("方法名",参数1,参数2,参数3....);
			object = client.invoke("getStudentObject", xml);
			System.out.println("返回数据:" + object[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 * 2：动态调用
	 * https://www.cnblogs.com/lichmama/p/8728262.html
	 */
	public static void main6() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//		Client client = dcf.createClient("http://127.0.0.1:8082/soap/demo?wsdl");
		Client client = dcf.createClient("http://127.0.0.1:8080/webservice/soap/demo?wsdl");
		// 需要密码的情况需要加上用户名和密码
		// client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
		Object[] object = new Object[0];
		try {
			Student student = new Student();
			student.setId(2);
			student.setName("wjggwm");
			// invoke("方法名",参数1,参数2,参数3....);
			object = client.invoke("getStudentObject1", student);
			System.out.println("返回数据:" + object[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testGetUser() {
		String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://server.webservice.webside.com/\"><soapenv:Header/><soapenv:Body><ser:getUser><arg0><name>wjggwm</name><sex>男</sex></arg0></ser:getUser></soapenv:Body></soapenv:Envelope>";
		String url = "http://127.0.0.1:8080/webservice/soap/demo?wsdl";
		String soapAction = "";
		String result = HttpUtil.doPostSoap1_1(url, xml, soapAction);
		System.out.println(result);
	}

}
